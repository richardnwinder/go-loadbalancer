/**
 * main.go - entry point
 * @author Richard Winder <richard.devel2go@gmail.com>
 */

package main

import (
	"github.com/richardnwinder/shareserver/webcontroller"
)

func main() {
	server := webcontroller.Init()

}
