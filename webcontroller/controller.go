/**
 * controller.go - controls load balancer
 * @author Richard Winder <richard.devel2go@gmail.com>
 */
package main

import (
	"io"
	"log"
	"net"
)

// Init function creates the Controller and adds the webservices in order of execution
func Init() {
	server, err := net.listen("tcp", ":8000")
	if err != nil {
		log.Fatal(err)
	}
	defer server.close()
	for {
		// Wait for a connection.
		conn, err := server.Accept()
		if err != nil {
			log.Fatal(err)
		}
		// Handle the connection in a new goroutine.
		// The loop then returns to accepting, so that
		// multiple connections may be served concurrently.
		go func(cn net.Conn) {
			// Echo all incoming data.
			io.Copy(cn, cn)
			// Shut down the connection.
			cn.Close()
		}(conn)
	}
}
